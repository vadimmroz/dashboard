import {useEffect, useState} from 'react'
import './App.css'
import {Link, Navigate, Route, Routes} from "react-router-dom";
import Login from "./pages/login/Login.jsx";
import Register from "./pages/register/Register.jsx";
import * as Realm from 'realm-web'
import Home from "./pages/home/Home.jsx";
import Header from "./components/header/Header.jsx";
import UserPage from "./pages/userPage/UserPage.jsx";

function App() {
    const [isLogin, setIsLogin] = useState(false)
    const [collection, setCollection] = useState(null)
    const [collectionUser, setCollectionUser] = useState(null)
    const [search, setSearch] = useState('')

    const handleSearch = (e) => {
        setSearch(e)
    }


    useEffect(() => {
        const login = async () => {
            const app = new Realm.App({id: 'data-cwaxy'})
            await app.logIn(Realm.Credentials.apiKey('I8gPHhSnNfT4KBrqUwuNew0jgfw17z3Z7BVU9C2oqrB8KUaxoJaMTLX6TjUDXbOm'))
            const client = app.currentUser.mongoClient('Cluster1')
            const collection = client.db('dashboards').collection('dashboards')
            setCollection(collection)
        }
        login()

    }, []);
    useEffect(() => {
        const login = async () => {
            const app = new Realm.App({id: 'data-cwaxy'})
            await app.logIn(Realm.Credentials.apiKey('I8gPHhSnNfT4KBrqUwuNew0jgfw17z3Z7BVU9C2oqrB8KUaxoJaMTLX6TjUDXbOm'))
            const client = app.currentUser.mongoClient('Cluster1')
            const coll = client.db('users').collection('users')
            setCollectionUser(coll)
        }
        login()
    }, [])


    return (
        <>

            <Routes>
                <Route path='userPage' element={<UserPage collectionUser={collectionUser}/>}/>
                <Route path='login' element={<Login collectionUser={collectionUser}/>}/>
                <Route path='register' element={<Register collectionUser={collectionUser}/>}/>
                <Route path='*' element={<Navigate to='login'/>}/>
                <Route path='home' element={<Home collectionUser={collectionUser} handleSearch={handleSearch}/>}/>
            </Routes>
            {isLogin && <Navigate to='login'/>}
        </>
    )
}

export default App
