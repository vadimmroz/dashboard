import React from 'react';
import classes from './userPage.module.css'
import Header from "../../components/header/Header.jsx";
import UserProfile from "../../components/userProfile/UserProfile.jsx";

const UserPage = ({collectionUser}) => {
    return (
        <>
            <Header collectionUser={collectionUser}/>
            <UserProfile collectionUser={collectionUser}/>
        </>
    );
};

export default UserPage;