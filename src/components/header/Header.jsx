import React, {useEffect, useState} from 'react';
import {NavLink, Link} from "react-router-dom";
import classes from './header.module.css';
import Profile from "./components/profile/Profile.jsx";
import {useMediaQuery} from 'react-responsive'
import SideMenu from "./components/sideMenu/SideMenu.jsx";
import ThemeChanger from "./components/themeChanger/ThemeChanger.jsx";

const Header = ({collectionUser, handleSearch}) => {

    const [profileVisible, setProfileVisible] = useState(false)
    const [closed, setClosed] = useState(false)
    const [userImg, setUserImg] = useState('')
    const [userName, setUserName] = useState('')
    const [side, setSide] = useState(false)
    const [searchVisible, setSearchVisible] = useState(true)
    const isBigScreen = useMediaQuery({query: '(max-width: 910px)'})

    useEffect(() => {
        if (collectionUser) {
            const a = async () => {
                const b = await collectionUser.findOne({user: localStorage.getItem('user')})
                setUserImg(b.img)
            }

            a()
        }
    }, [collectionUser]);
    useEffect(() => {
        if (collectionUser) {
            const a = async () => {
                const b = await collectionUser.findOne({user: localStorage.getItem('user')})
                setUserName(b.user)
            }

            a()
        }
    }, [collectionUser]);

    const handleProfilelVisible = () => {
        if (profileVisible) {
            setClosed(true)
            setTimeout(() => {
                setProfileVisible(!profileVisible)
                setClosed(false)
            }, 500)
        } else {
            setProfileVisible(!profileVisible)
        }
    }
    const handleSide = () => {
        if (side) {
            setSide(false)
        } else {
            setSide(true)
        }
    }
    const handleSearchVisible = () => {
        // if (searchVisible) {
        //     setSearchVisible(false)
        // } else {
        //     setSearchVisible(true)
        // }
        setSearchVisible(!searchVisible)
    }

    return (
        <header>
            {isBigScreen && <button
                className={classes.sideMenuBtn}
                onClick={handleSide}>
                <img src="https://i.ibb.co/mGt1WsF/menu-regular-24.png" alt=""/>
            </button>}
            <Link to='/home'><img src="https://jira.cbt.ua/images/atlassian-jira-logo-large.png" alt=""
                                  className={classes.logo}/></Link>

            <nav>
                <NavLink to='/home'>Your Work</NavLink>
                <NavLink to='/dashboards'>Dashboards</NavLink>
                <NavLink to='/people'>People</NavLink>
                <button>Create</button>
            </nav>
            <div className={classes.input}>
                <img src="https://i.ibb.co/Rb2fQHc/search-regular-24-1.png" alt=""/>
                <input type="search" placeholder='Search' onChange={event => handleSearch(event.target.value)}/>
            </div>
            {/* Search by modal window))))))) */}
            <div className={classes.hidden} onClick={handleSearchVisible}>
                <img src="https://i.ibb.co/Rb2fQHc/search-regular-24-1.png" alt=""/>
            </div>
            <div
                className={searchVisible ? (classes.input_ + ' ' + classes.down) : (classes.input_ + ' ' + classes.up)}>
                <img src="https://i.ibb.co/Rb2fQHc/search-regular-24-1.png" alt=""/>
                <input type="search" placeholder='Search' onChange={event => handleSearch(event.target.value)}/>
            </div>
            <div className={searchVisible ? (classes.fetch + ' ' + classes.hidden_fetch) : (classes.fetch)}
                 onClick={handleSearchVisible}>
            </div>
            {/* Search by modal window))))))) */}
            <div className={classes.userBtns}>
                <img src="https://i.ibb.co/LN59TqX/bell-solid-24.png" alt=""/>
                <img src="https://i.ibb.co/PTcBW3Q/question-mark-regular-24.png" alt=""/>
                <ThemeChanger></ThemeChanger>
                <img src={userImg} alt={userImg} className={classes.userImg} onClick={handleProfilelVisible}/>

                {profileVisible &&
                    <Profile closed={closed} collectionUser={collectionUser} userImg={userImg}
                             handleProfilelVisible={handleProfilelVisible} userName={userName}/>}
            </div>
            <SideMenu side={side} handleSide={handleSide}/>
        </header>
    );
};

export default Header;