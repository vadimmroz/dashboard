import React, {useEffect, useState} from 'react';
import classes from './profile.module.css'
import {Navigate, Route, Routes} from "react-router-dom";

const Profile = ({closed, userImg, userName, handleProfilelVisible}) => {

    const [toProfile, setToProfile] = useState(false)
    const [logOut, setLogOut] = useState(false)

    const handleTo = () => {
        setToProfile(true)
    }
    const handleLogOut = () => {
        setLogOut(true)
        localStorage.removeItem('user')
        localStorage.removeItem('pass')
    }


    return (
        <>
            <div
                className={closed ? (classes.modalProfile + '  ' + classes.closed) : (classes.modalProfile + ' ' + classes.opened)}>
                <img src={userImg} alt={userImg} className={classes.profileImg}/>
                <p>@{userName}</p>
                <button className={classes.profile} onClick={handleTo}>Profile</button>
                <button className={classes.logOut} onClick={handleLogOut}>Log Out</button>
                <h3 className={classes.version}>
                    version 1.2
                </h3>
            </div>
            {toProfile && <Navigate to={'/userPage'}/>}
            {logOut && <Navigate to={'/login'}/>}
            <div className={classes.fetch} onClick={handleProfilelVisible}>

            </div>
        </>
    );
};

export default Profile;