import React from 'react';
import classes from "./sideMenu.module.css";
import {NavLink} from "react-router-dom";

const SideMenu = ({side, handleSide}) => {
    return (
        <>
            <div className={side ? (classes.sideMenu + ' ' + classes.goRight) : (classes.sideMenu + ' ' + classes.goLeft)}>
                <NavLink to='/home'>Your Work</NavLink>
                <NavLink to='/dashboards'>Dashboards</NavLink>
                <NavLink to='/people'>People</NavLink>
                <button>Create</button>
            </div>
            <div className={side ? (classes.fetch) : (classes.fetch + ' ' + classes.hidden)} onClick={handleSide}>

            </div>
        </>
    );
};

export default SideMenu;