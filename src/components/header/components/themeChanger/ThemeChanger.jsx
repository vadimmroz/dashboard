import React, {useState} from 'react';
import classes from './themeChanger.module.css';
import {useTheme} from '../../../../hooks/useTheme.js'
import classNames from "classnames";

const ThemeChanger = () => {

    const {theme, setTheme} = useTheme()
    //
    const handleTheme = (e) => {
        if (theme === 'light') {
            setTheme('dark')
        } else {
            setTheme('light')
        }
    }


    return (
        <>
            <div className={classes.container}>
                <label className={classes.switch}>
                    <input type="checkbox" className={theme === 'dark' ? classNames(classes.box, classes.Checked) : classNames(classes.box)} onClick={handleTheme} id={'slider'}/>
                    <span
                        className={classNames(classes.slider, classes.round)}/>
                </label>
            </div>
        </>
    );
};

export default ThemeChanger;