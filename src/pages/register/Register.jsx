import React, {useState} from 'react';
import classes from './register.module.css'
import {Link, Navigate} from "react-router-dom";

const Register = ({collectionUser}) => {

    const [register, setRegister] = useState({
        user: '',
        pass: '',
        dashGuest: [''],
        dash: [''],
        img: '',
        id: (Math.random() * (999999 - 2) + 2).toString()
    })
    const [isReg, setIsReg] = useState(false)
    const reg = async () => {
        if (register.user && register.pass) {
            const a = await collectionUser.findOne({user: register.user})
            if (!a) {
                collectionUser.insertOne(register)
                setIsReg(true)
                localStorage.setItem('user', register.user)
                localStorage.setItem('pass', register.pass)
            }
        }
    }

    return (
        <div className={classes.register}>
            <div className={classes.popup}>
                <h1>
                    Register
                </h1>
                <input placeholder='Login' type='text'
                       onChange={e => setRegister({...register, user: e.target.value})}/>
                <input placeholder='Password' type='password'
                       onChange={e => setRegister({...register, pass: e.target.value})}/>

                <div className={classes.sideMenu}>
                    <button onClick={() => reg()}>Register</button>
                    <Link to='/login'>Login</Link>
                </div>
                {isReg && <Navigate to='/home'/>}
            </div>
        </div>
    );
};

export default Register;