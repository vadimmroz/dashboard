import React, {useEffect, useState} from 'react';
import classes from './login.module.css'
import {Link, Navigate} from "react-router-dom";

const Login = ({collectionUser}) => {
    const [login, setLogin] = useState({
        user: '',
        pass: ''
    })
    const [isLogin, setIsLogin] = useState(false)
    const handleButton = async () => {
        if (collectionUser) {
            const a = await collectionUser.findOne({name: login.name, pass: login.pass})
            if (a) {
                localStorage.setItem('user', login.user)
                localStorage.setItem('pass', login.pass)
                setIsLogin(true)
            }
        }
    }

    return (
        <div className={classes.login}>
            <div className={classes.popup}>
                <h1>
                    Login
                </h1>
                <input placeholder='Login' type='text' onChange={e => setLogin({...login, user: e.target.value})}/>
                <input placeholder='Password' type='password'
                       onChange={e => setLogin({...login, pass: e.target.value})}/>

                <div className={classes.sideMenu}>
                    <button onClick={() => handleButton()}>Login</button>
                    <Link to='/register'>Register</Link>
                </div>
                {isLogin && <Navigate to='/home'/>}
            </div>
        </div>
    );
};

export default Login;