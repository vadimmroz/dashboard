import React from 'react';
import classes from './home.module.css'
import Header from "../../components/header/Header.jsx";
import Profile from "../../components/header/components/profile/Profile.jsx";

const Home = ({collectionUser, handleSearch}) => {
    return (
        <>
            <Header collectionUser={collectionUser} handleSearch={handleSearch}/>
            <div className={classes.home}>

            </div>
        </>
    );
};

export default Home;