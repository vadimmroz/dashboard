import React, {useState} from 'react';
import classes from './userButtons.module.css'
import {Navigate} from "react-router-dom";

const UserButtons = () => {
    const [logOut, setLogOut] = useState(false)

    const handleLogOut = () => {
        setLogOut(true)
        localStorage.removeItem('user')
        localStorage.removeItem('pass')
    }

    return (
        <>
            <div className={classes.container}>
                <button className={classes.Button1} ><img src="https://i.ibb.co/hcdSH9Z/pencil-regular-24.png" alt=""/></button>
                <button className={classes.Button2} onClick={handleLogOut} ><img src="https://i.ibb.co/QMZq21y/log-out-regular-24-1.png" alt=""/></button>
            </div>

            {logOut && <Navigate to={'/login'}/>}
        </>
    );
};

export default UserButtons;