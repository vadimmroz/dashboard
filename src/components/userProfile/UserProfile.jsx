import React, {useEffect, useState} from 'react';
import classes from './userProfile.module.css'
import Wall from './components/wall/Wall.jsx'
import UserButtons from './components/userButtons/UserButtons.jsx'

const UserProfile = ({collectionUser}) => {

    const [userImg, setUserImg] = useState('')

    useEffect(() => {
        if (collectionUser) {
            const a = async () => {
                const b = await collectionUser.findOne({user: localStorage.getItem('user')})
                setUserImg(b.img)
            }

            a()
        }

    }, [collectionUser]);

    const [userName, setUserName] = useState('')

    useEffect(() => {
        if (collectionUser) {
            const a = async () => {
                const b = await collectionUser.findOne({user: localStorage.getItem('user')})
                setUserName(b.user)
            }

            a()
        }

    }, [collectionUser]);

    return (
        <>
            <div className={classes.container}>
                <img src={userImg} alt='' className={classes.userImg}/>
                <h2 className={classes.userName}>{"@" + userName}</h2>
                <Wall/>
                <UserButtons/>
            </div>


        </>
    );
};

export default UserProfile;